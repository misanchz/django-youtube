from django.db import models

class Selected(models.Model):
    name = models.CharField(max_length=64)
    link = models.CharField(max_length=64)
    videoid = models.CharField(max_length=64)
    image = models.CharField(max_length=64)
    published = models.CharField(max_length=64)
    descr = models.TextField()

class Selectable(models.Model):
    name = models.CharField(max_length=64)
    link = models.CharField(max_length=64)
    videoid = models.CharField(max_length=64)
    image = models.CharField(max_length=64)
    published = models.CharField(max_length=64)
    descr = models.TextField()

class Channel(models.Model):
    name = models.CharField(max_length=64)
    link = models.CharField(max_length=64)
