from django.shortcuts import render
from .models import Selected, Selectable, Channel
from django.views.decorators.csrf import csrf_exempt
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib
import string
from django.http import HttpResponse

counter = 0

def addSelectable(name, link, id, img, date, descr):
    try:
        video = Selectable.objects.get(name=name, link=link, videoid=id, image=img, published=date, descr=descr)
        video.name = name
        video.link = link
        video.videoid = id
        video.image = img
        video.published = date
        video.descr = descr
    except Selectable.DoesNotExist:
        video = Selectable(name=name, link=link, videoid=id, image=img, published=date, descr=descr)
    video.save()

def addSelected(name, link, id, img, date, descr):
    try:
        video = Selected.objects.get(name=name, link=link, videoid=id, image=img, published=date, descr=descr)
        video.name = name
        video.link = link
        video.videoid = id
        video.image = img
        video.published = date
        video.descr = descr
    except Selected.DoesNotExist:
        video = Selected(name=name, link=link, videoid=id, image=img, published=date, descr=descr)
    video.save()

def parser():
    class YTHandler(ContentHandler):

        def __init__ (self):
            self.inEntry = False
            self.inContent = False
            self.inAuthor = False
            self.content = ""
            self.title = ""
            self.link = ""
            self.id = ""
            self.img = ""
            self.channel = ""
            self.ch_link = ""
            self.date = ""
            self.descr = ""

        def startElement (self, name, attrs):
            if name == 'entry':
                self.inEntry = True

            elif name == 'author':
                self.inAuthor = True

            elif self.inEntry:
                if name == 'title':
                    self.inContent = True
                elif name == 'yt:videoId':
                    self.inContent = True
                elif name == 'published':
                    self.inContent = True
                elif name == 'media:description':
                    self.inContent = True
                elif name == 'link':
                    self.link = attrs.get('href')
                elif name == 'media:thumbnail':
                    self.img = attrs.get('url')

            elif self.inAuthor:
                if name == 'name':
                    self.inContent = True
                elif name == 'uri':
                    self.inContent = True

        def endElement (self, name):
            if name == 'entry':
                self.inEntry = False
                addSelectable(self.title, self.link, self.id, self.img, self. date ,self.descr)

            elif name == 'author':
                self.inAuthor = False
                try:
                    channel = Channel.objects.get(name=self.channel, link=self.ch_link)
                    channel.name = self.channel
                    channel.link = self.ch_link
                except Channel.DoesNotExist:
                    channel = Channel(name=self.channel, link=self.ch_link)
                channel.save()

            elif self.inAuthor:
                if name == 'name':
                    self.channel = self.content
                elif name == 'uri':
                    self.ch_link = self.content

                self.content = ""
                self.inContent = False

            elif self.inEntry:
                if name == 'title':
                    self.title = self.content
                elif name == 'yt:videoId':
                    self.id = self.content
                elif name == 'published':
                    self.date = self.content
                elif name == 'media:description':
                    self.descr = self.content

                self.content = ""
                self.inContent = False

        def characters (self, chars):
            if self.inContent:
                self.content = self.content + chars

    Parser = make_parser()
    Parser.setContentHandler(YTHandler())

    url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
    xmlStream = urllib.request.urlopen(url)
    Parser.parse(xmlStream)

@csrf_exempt
def video_lists(request):
    global counter

    #Controlar que solo se analiza el documento XML una vez
    if counter == 0:
        parser()
    counter = counter + 1

    if request.method == 'POST':
        action = request.POST['action']

        #Borramos video de la lista seleccionables y lo añadimos a la lista de videos seleccionados
        if action == 'Seleccionar':
            id = request.POST['ytid']
            selected_video = Selectable.objects.get(videoid=id)
            name = selected_video.name
            link = selected_video.link
            videoid = selected_video.videoid
            image = selected_video.image
            date =  selected_video.published
            description = selected_video.descr
            selected_video.delete()

            addSelected(name, link, videoid, image, date, description)

        #Borramos video de la lista seleccionados y lo añadimos a la lista de videos seleccionables
        elif action == 'Eliminar':
            id = request.POST['ytid']
            unselected_video = Selected.objects.get(videoid=id)
            name = unselected_video.name
            link = unselected_video.link
            videoid = unselected_video.videoid
            image = unselected_video.image
            date =  unselected_video.published
            description = unselected_video.descr
            unselected_video.delete()

            addSelectable(name, link, videoid, image, date, description)

    selectable_list = Selectable.objects.all()
    selected_list = Selected.objects.all()
    context = {'selectable_list': selectable_list, 'selected_list': selected_list}
    return render(request, 'yt/lists.html', context)

def descr_video(request, id):
    try:
        video = Selected.objects.get(videoid=id)
        channel = Channel.objects.first()
        context = {'video': video, 'channel': channel}
        return render(request, 'yt/descr.html', context)
    except Selected.DoesNotExist:
        return render(request, 'yt/error.html')
