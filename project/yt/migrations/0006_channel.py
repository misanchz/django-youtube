# Generated by Django 3.0.3 on 2020-04-10 16:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('yt', '0005_delete_channel'),
    ]

    operations = [
        migrations.CreateModel(
            name='Channel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('link', models.CharField(max_length=64)),
            ],
        ),
    ]
