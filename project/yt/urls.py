from django.urls import path
from . import views

urlpatterns = [
  path('', views.video_lists),
  path('<str:id>', views.descr_video)
]
